/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.adfs.client;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedHashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.samply.auth.client.jwt.JWTAdfsAccessToken;
import de.samply.auth.client.jwt.JWTException;
import de.samply.auth.client.jwt.KeyLoader;

/**
 * A simple ADFS client.
 */
public class AdfsClient {

    private static Logger logger = LogManager.getLogger(AdfsClient.class);

    /**
     * The ADFS URL, e.g. 'https://adfs.example.org/' or 'https://adfs.example.org/adfs/oauth2'.
     */
    private String url;

    /**
     * The ADFS public key that is used to verify the signature from the access token.
     */
    private String publicKey;

    /**
     * The Resource that is requested from the ADFS.
     */
    private String resource;

    /**
     * Your client ID.
     */
    private String clientId;

    /**
     * The HTTP client that will be used.
     */
    private Client client;

    /**
     * Initializes a new ADFS client.
     * @param url
     * @param publicKey
     * @param resource
     * @param clientId
     * @param client
     */
    public AdfsClient(String url, String publicKey, String resource, String clientId, Client client) {
        this.setUrl(url);
        this.publicKey = publicKey;
        this.resource = resource;
        this.clientId = clientId;
        this.client = client;
    }

    /**
     * Returnes the redirect URL to the ADFS.
     * @param localRedirectUrl
     * @return
     */
    public String getRedirectUrl(String localRedirectUrl) {
        StringBuilder builder = new StringBuilder(getBaseUrl());

        builder.append("authorize?");
        try {
            builder.append("redirect_uri=").append(URLEncoder.encode(localRedirectUrl, StandardCharsets.UTF_8.displayName()));
            builder.append("&client_id=").append(URLEncoder.encode(clientId, StandardCharsets.UTF_8.displayName()));
            builder.append("&resource=").append(URLEncoder.encode(resource, StandardCharsets.UTF_8.displayName()));
            builder.append("&response_type=code");

            logger.debug("Redirect URL: " + builder.toString());

            return builder.toString();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Returns the base URL of the ADFS. appends a trailing slash if necessary, or
     * the '/adfs/oauth2/' part.
     * @return
     */
    private String getBaseUrl() {
        StringBuilder builder = new StringBuilder(url);
        if(!url.endsWith("/")) {
            builder.append("/");
        }

        if(!url.endsWith("/adfs/oauth2") &&
                !url.endsWith("/adfs/oauth2/")) {
            builder.append("adfs/oauth2/");
        }
        return builder.toString();
    }

    /**
     * Gets a new access token from the ADFS server.
     * @param code
     * @param localRedirectUrl
     * @return
     * @throws JWTException
     */
    public JWTAdfsAccessToken getAccessToken(String code, String localRedirectUrl) throws JWTException {
        WebTarget target = client.target(getBaseUrl() + "token");

        MultivaluedHashMap<String, String> values = new MultivaluedHashMap<String, String>();
        values.add("grant_type", "authorization_code");
        values.add("client_id", clientId);
        values.add("redirect_uri", localRedirectUrl);
        values.add("code", code);

        AdfsAccessTokenDTO post = target.request(MediaType.APPLICATION_JSON_TYPE)
                    .post(Entity.entity(values, MediaType.APPLICATION_FORM_URLENCODED_TYPE), AdfsAccessTokenDTO.class);

        return new JWTAdfsAccessToken(KeyLoader.loadKey(publicKey), post.getAccessToken());
    }

    /**
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url the url to set
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * @return the publicKey
     */
    public String getPublicKey() {
        return publicKey;
    }

    /**
     * @param publicKey the publicKey to set
     */
    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }

    /**
     * @return the resource
     */
    public String getResource() {
        return resource;
    }

    /**
     * @param resource the resource to set
     */
    public void setResource(String resource) {
        this.resource = resource;
    }

    /**
     * @return the clientId
     */
    public String getClientId() {
        return clientId;
    }

    /**
     * @param clientId the clientId to set
     */
    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

}
